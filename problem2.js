/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

// require fs
const fs = require('fs')
const path = require('path')


// Reading files generic funtion:

function readFileLipsum(dir, callback){
    fs.readFile(dir, "utf-8", (err, data)=>{
        if(err){
            console.log(err)
        }else{
            console.log('Reading Successful')
            const dataConverter = data.toString().toUpperCase()
            callback(dataConverter)
        }
    })
}

// Reading file from upper writing to upper generic write funtion:

function readFileUpper(dir, callback){
    fs.readFile(dir, "utf-8", (err, data)=>{
        if(err){
            console.log(err)
        }else{
            console.log('Reading Successful')
            const lowerString = data.toString().toLowerCase().split(".").join(".\n")
            const splitArray = lowerString.toString()
            callback(splitArray)
        }
    })
}


// Reading file from lower writing to sort generic write funtion:

function readFileLower(dir, callback){
    fs.readFile(dir, "utf-8", (err, data)=>{
        if(err){
            console.log(err)
        }else{
            console.log('Reading Successful')
            const dataArr = data.split(".").sort()
            const dataStr = data.toString()
            callback(dataStr)
        }
    })
}



// Writing files generic function:

function writeFile(dir, data, callback){
    fs.writeFile(dir, data, "utf-8", (err)=>{
        if(err){
            console.log(err)
        }else{
            console.log('writing successful')
            callback()
        }
    })
}


// Writing filenames 

function filenamesStore(dir, data, callback){
    fs.appendFile(dir, data, (err) => {
        if(err){
            console.log(err)
        }else{
            console.log('Appending filenames successful')
            callback()
        }
    }) 
}


// Deleting files

function deleteFiles(dir) {
    let files = fs.readFile(dir, "utf-8", (err,data) => {
        if(err){
            console.log(err)
        }else{
            let arr = data.split("\n")
            console.log(arr)
            arr.forEach((file) => {
                fs.unlink('../' +file, (err) => {
                    if (err){
                        console.log(err)
                    }else{
                        console.log('Deleted files')
                    }
                })
            } )
            
        }
    });
    
  }



// The main function:

function problem2(){
    const dir = path.join(__dirname);
    readFileLipsum(dir + '/'+ 'lipsum.txt', (data) => {
        writeFile(dir + '/' + 'lipsumUpper.txt', data, ()=>{
            writeFile(dir + '/' + 'filenames.txt', 'lipsumUpper.txt', () => {
                readFileUpper(dir + '/' + 'lipsumUpper.txt', (data) => {
                    writeFile(dir + '/' + 'lipsumLower.txt', data, () => {
                        filenamesStore(dir + '/' + 'filenames.txt', "\n" + 'lipsumLower.txt', () => {
                            readFileLower(dir + '/' + 'lipsumLower.txt', (data) => {
                                writeFile(dir + '/' + 'lipsumSorted.txt', data, () => {
                                    filenamesStore(dir + '/' + 'filenames.txt', "\n" + 'lipsumSorted.txt', () => {
                                        deleteFiles(dir + '/' + 'filenames.txt')
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
}
module.exports = problem2;



// Reading data from file --> converting to Upper case --> writing the data in a new file:

// function toUpper(readPath, writePath, fileNamesPath) {
//     fs.readFile(readPath, (err, data) => {
//         if (err) {
//             console.log('Error:')
//             console.log(err)
//         } else {
//             console.log('Reading successful')
//             const dataConverter = data.toString().toUpperCase()

//             fs.writeFile(writePath, dataConverter, "utf-8", callback)
//             console.log("Writing complete")

//             fs.writeFile(fileNamesPath, writePath + "\n", callback)
//             console.log("Writing complete")
//         }
//     })
// }


// // Reading the new file --> converting it to lower case --> then split its contents into sentenses --> write it to a new file:

// function toLowerThenSplit(readPath, writePath) {
//     fs.readFile(readPath, (err, data) => {
//         if (err) {
//             console.log('Error:')
//             console.log(err)
//         } else {
//             console.log('Reading successful')
//             const lowerString = data.toString().toLowerCase().split(".").join(".\n")
//             const splitArray = lowerString.toString()

//             fs.writeFile(writePath, splitArray, "utf-8", callback)
//             console.log("Writing complete")

//             fs.appendFile('/home/akshay/Desktop/Callbacks Drill/filenames.txt', writePath + "\n", "utf-8", callback)
//         }
//     })
// }


// // Read the new file --> sort the content --> write it out new file:

// function readSortWrite(readPath, writePath) {
//     fs.readFile(readPath, "utf-8", (err, data) => {
//         if (err) {
//             console.log('Error:')
//             console.log(err)
//         } else {
//             console.log('Reading successful')
//             const newData = data
//             const dataArr = newData.split(".").sort()
//             const dataStr = dataArr.toString()

//             fs.writeFile(writePath, dataStr, "utf-8", callback)
//             console.log("Writing Complete")

//             fs.appendFile('/home/akshay/Desktop/Callbacks Drill/filenames.txt', writePath + "\n", "utf-8", callback)
//         }
//     })
// }


// // Reading all the new file names --> Then deleting them:

// function readAndDelete(readPath) {
//     fs.readFile(readPath, "utf-8", (err, data) => {
//         if (err) {
//             console.log('Error:')
//             console.log(err)
//         } else {
//             const arr = data.split("\n")

//             function deleteFiles(files, callback) {
//                 const i = files.length;
//                 files.map(function (filepath) {
//                     fs.unlink(filepath, function (err) {
//                         i--;
//                         if (err) {
//                             callback(err);
//                             return;
//                         } else if (i <= 0) {
//                             callback(null);
//                         }
//                     });
//                 });
//             }

//             deleteFiles(arr, (err) => {
//                 if (err) {
//                     console.log('err')
//                 } else {
//                     console.log('all files removed')
//                 }
//             })

//         }
//     })
// }


// module.exports = { toUpper, toLowerThenSplit, readSortWrite, readAndDelete };