const fs = require("fs");
const path = require("path");



/*Creating a directory*/

function createDirctory(dir, callback) {
  fs.mkdir(dir, function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Directory created");
      callback();
    }
  });
}



/*Creating a json file*/

function createJson(pathName1, data, callback) {
  fs.writeFile(pathName1, data, "utf-8", function (data, err) {
    if (data) {
      console.log("err");
    } else {
      console.log("File created successfully");
      callback();
    }
  });
}



// Deleting files aumatically:

function deleteFiles(dirName){
  let dirPath = path.join(__dirname,dirName)
  let fileNames = fs.readdirSync(dirPath)
  let deleteContents = fileNames.map((files) => {
    let filePath = path.join(dirPath, files)
    // console.log(filePath)
    fs.unlink(filePath, (err) => {
      if(err){
        console.log(err)
      }else{
        console.log('Files deleted')
      }
    })
  })
}



function problem1() {
  const dir = path.join(__dirname, "dir1/");
  // console.log(pathFile);
  createDirctory(dir, () => {
    createJson(`${dir}file1.json`, 'Hi', () => {
      createJson(`${dir}file2.json`, 'There', () => {
        createJson(`${dir}file3.json`, '!', () => {
          createJson(`${dir}file4.json`, 'How are', () => {
            createJson(`${dir}file5.json`, 'you?', ()=>  {
              // console.log("hello")
              deleteFiles("dir1");
            });
          });
        });
      });
    });
  });
}
// problem1()
module.exports = problem1